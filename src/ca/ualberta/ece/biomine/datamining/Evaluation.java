package ca.ualberta.ece.biomine.datamining;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class Evaluation {
    public String actualFprThres;
    public String scoreThres;

    public String auc;
    public String aucLowFPR;
    public final double fprRange = 0.1;
    public String ratio;

    public String accuracy;
    public String precision;
    public String sensitivity;
    public String specificity;
    public String mcc;
    /*
     * @param listScores: list of ScoreNoBinary type objects containing the native class label and predicted score for the positive label (1)
     * @param thresType: defines the way to generate binary predictions:
     * "fpr": generate binary prediction by a given false positive rate. In this case the "thres" parameter is used as the false positive rate
     * "equal": generate binary prediction by a score threshold such that the predicted number of positives equals to the actual number of positives. In this case the "thres" parameter is not used
     * "score": generate binary prediction by a given score threshold. In this case the "thres" parameter is used as the given score threshold
     * @param thres: false positive rate or the given score threshold
     */
    public Evaluation(ArrayList<ScoreNoBinary> listScores, String thresType, double thres) throws Exception{
    	evaNoBinary(listScores, thresType, thres);
    }// construction
    
    private void evaNoBinary(ArrayList<ScoreNoBinary> listScores, String thresType, double thres){
        // calculate auc
        Collections.sort(listScores);
        // remove duplicated predictions
        Set<Double> setPredictions = new LinkedHashSet<Double>();
        for (ScoreNoBinary curScore : listScores) {
            setPredictions.add(curScore.scoreForPos);
        }

        if (setPredictions.size() <= 1) {
            this.scoreThres = "NA";
            this.accuracy = "NA";
            this.precision = "NA";
            this.sensitivity = "NA";
            this.specificity = "NA";
            this.mcc = "NA";
            this.auc = "NA";
            return;
        }

        Double[] arrayPredictions = new Double[setPredictions.size()];
        setPredictions.toArray(arrayPredictions);
        // calculate threshold
        double[] thresholdTemp = new double[arrayPredictions.length - 1];
        for (int i = 0; i < thresholdTemp.length; i++) {
            thresholdTemp[i] = (arrayPredictions[i] + arrayPredictions[i + 1]) / 2;
        }
        // calculate fpr and tpr on different threshold
        double[] threshold = new double[thresholdTemp.length + 2];
        for (int i = 1; i < threshold.length - 1; i++) {
            threshold[i] = thresholdTemp[i - 1];
        }
        threshold[0] = Math.max(1, thresholdTemp[0]);
        threshold[threshold.length - 1] = Math.min(0, thresholdTemp[thresholdTemp.length - 1]);
        double[] tpr = new double[threshold.length];
        double[] fpr = new double[threshold.length];
        double[] tp = new double[threshold.length];
        double[] tn = new double[threshold.length];
        double[] fp = new double[threshold.length];
        double[] fn = new double[threshold.length];
        double[] numPosPred = new double[threshold.length];

        // score threshold from high to low
        for (int i = 0; i < threshold.length; i++) {
            double[] newPredictions = new double[listScores.size()];

            for (int j = 0; j < newPredictions.length; j++) {
                if (listScores.get(j).scoreForPos >= threshold[i]){
                    newPredictions[j] = 1; // 1 means true
                    numPosPred[i] ++;
                }
                else {
                    newPredictions[j] = 0; // 0 means false
                }
            }// for j, get new prediction

            for (int j = 0; j < newPredictions.length; j++) {
                if (listScores.get(j).actual == 1) {
                    if (newPredictions[j] == 1)
                        tp[i] ++;
                    else
                        fn[i] ++;
                }// if actual == 1, true
                else {
                    if (newPredictions[j] == 0)
                        tn[i] ++;
                    else
                        fp[i] ++;
                } // if actual == 0, false
            }// for j, get tp, tn, fp, fn under current threshold

            tpr[i] = tp[i] / (tp[i] + fn[i]);
            fpr[i] = fp[i] / (fp[i] + tn[i]);
        }// for different threshold, i

        // get AUC
        double dAuc = 0.0;
        for (int i = 0; i < tpr.length - 1; i++) {
            dAuc += (tpr[i + 1] + tpr[i]) * (fpr[i + 1] - fpr[i]) / 2;
        }
        this.auc = String.valueOf(dAuc);

         // get AUC in low FPR range
        int k;
        int actualInd;
        for (k = 0; k < fpr.length; k++) {
            if (fpr[k] >= fprRange)
                break;
        }// for different FPR
        if (fpr[k] == fprRange)
            actualInd = k;
        else
            actualInd = (fprRange - fpr[Math.max(0, k-1)]) < (fpr[Math.min(fpr.length - 1, k+1)] - fprRange) ? Math.max(0, k-1) : Math.min(fpr.length - 1, k+1);
        double dLowAUC = 0.0;
        for(int i = 0; i < actualInd; i ++){
            dLowAUC += (tpr[i + 1] + tpr[i]) * (fpr[i + 1] - fpr[i]) / 2;
        }
        this.aucLowFPR = String.valueOf(dLowAUC);
        this.ratio = String.valueOf(dLowAUC/(fpr[actualInd] * fpr[actualInd] / 2.0));

        switch (thresType) {
            //region case fpr
            case "fpr":
                // find the actual index of score threshold where the FPR is closest to the thres
                for (k = 0; k < fpr.length; k++) {
                    if (fpr[k] >= thres)
                        break;
                }// for different FPR
                if (fpr[k] == thres)
                    actualInd = k;
                else
                    actualInd = (thres - fpr[Math.max(0, k-1)]) < (fpr[Math.min(fpr.length - 1, k+1)] - thres) ? Math.max(0, k-1) : Math.min(fpr.length - 1, k+1);

                // actual fpr threshold and score threshold
                this.actualFprThres = String.valueOf(fpr[actualInd]);
                this.scoreThres = String.valueOf(threshold[actualInd]);

                if ((tp[actualInd] + fp[actualInd] + fn[actualInd] + tn[actualInd]) != 0) {
                    this.accuracy = String.valueOf((tp[actualInd] + tn[actualInd])
                            / (tp[actualInd] + fp[actualInd] + fn[actualInd] + tn[actualInd]));
                } else {
                    this.accuracy = "NA";
                }
                if ((tp[actualInd] + fp[actualInd]) != 0) {
                    this.precision = String.valueOf((tp[actualInd] / (tp[actualInd] + fp[actualInd])));
                } else {
                    this.precision = "NA";
                }
                if ((tp[actualInd] + fn[actualInd]) != 0) {
                    this.sensitivity = String.valueOf((tp[actualInd] / (tp[actualInd] + fn[actualInd])));
                } else {
                    this.sensitivity = "NA";
                }
                if ((fp[actualInd] + tn[actualInd]) != 0) {
                    this.specificity = String.valueOf((tn[actualInd] / (fp[actualInd] + tn[actualInd])));
                } else {
                    this.specificity = "NA";
                }
                if ((tp[actualInd] + fp[actualInd]) * (tp[actualInd] + fn[actualInd]) * (tn[actualInd] + fp[actualInd])* (tn[actualInd] + fn[actualInd]) != 0) {
                    this.mcc = String.valueOf(((tp[actualInd] * tn[actualInd] - fp[actualInd] * fn[actualInd]) / Math
                            .sqrt((tp[actualInd] + fp[actualInd]) * (tp[actualInd] + fn[actualInd]) * (tn[actualInd] + fp[actualInd])
                                    * (tn[actualInd] + fn[actualInd]))));
                } else {
                    this.mcc = "NA";
                }
                break;
            //endregion case fpr
            //region case equal
            case "equal":
                int numNativePos = 0;
                for(int i = 0; i < listScores.size(); i ++){
                    if(listScores.get(i).actual == 1.0){
                        numNativePos ++;
                    }
                }
                // find the actual index of score threshold where the #predicted positives == #native positives
                for (k = 0; k < numPosPred.length; k ++) {
                    if (numPosPred[k] >= numNativePos)
                        break;
                }// for different FPR
                if (numPosPred[k] == numNativePos)
                    actualInd = k;
                else
                    actualInd = (numNativePos - numPosPred[Math.max(0, k-1)]) < (numPosPred[Math.min(numPosPred.length - 1, k+1)] - numNativePos) ? Math.max(0, k-1) : Math.min(numPosPred.length - 1, k+1);

                // actual fpr threshold and score threshold
                this.actualFprThres = String.valueOf(fpr[actualInd]);
                this.scoreThres = String.valueOf(threshold[actualInd]);

                if ((tp[actualInd] + fp[actualInd] + fn[actualInd] + tn[actualInd]) != 0) {
                    this.accuracy = String.valueOf((tp[actualInd] + tn[actualInd])
                            / (tp[actualInd] + fp[actualInd] + fn[actualInd] + tn[actualInd]));
                } else {
                    this.accuracy = "NA";
                }
                if ((tp[actualInd] + fp[actualInd]) != 0) {
                    this.precision = String.valueOf((tp[actualInd] / (tp[actualInd] + fp[actualInd])));
                } else {
                    this.precision = "NA";
                }
                if ((tp[actualInd] + fn[actualInd]) != 0) {
                    this.sensitivity = String.valueOf((tp[actualInd] / (tp[actualInd] + fn[actualInd])));
                } else {
                    this.sensitivity = "NA";
                }
                if ((fp[actualInd] + tn[actualInd]) != 0) {
                    this.specificity = String.valueOf((tn[actualInd] / (fp[actualInd] + tn[actualInd])));
                } else {
                    this.specificity = "NA";
                }
                if ((tp[actualInd] + fp[actualInd]) * (tp[actualInd] + fn[actualInd]) * (tn[actualInd] + fp[actualInd])* (tn[actualInd] + fn[actualInd]) != 0) {
                    this.mcc = String.valueOf(((tp[actualInd] * tn[actualInd] - fp[actualInd] * fn[actualInd]) / Math
                            .sqrt((tp[actualInd] + fp[actualInd]) * (tp[actualInd] + fn[actualInd]) * (tn[actualInd] + fp[actualInd])
                                    * (tn[actualInd] + fn[actualInd]))));
                } else {
                    this.mcc = "NA";
                }
                break;
            //endregion case equal
            case "score":
                //region case score
                double iTp = 0, iTn = 0, iFp = 0, iFn = 0;
                double[] newPreditions = new double[listScores.size()];

                for (int j = 0; j < newPreditions.length; j++) {
                    if (listScores.get(j).scoreForPos >= thres)
                        newPreditions[j] = 1; // 1 means true
                    else
                        newPreditions[j] = 0; // 0 means false
                }// for j, get new prediction

                for (int j = 0; j < newPreditions.length; j++) {
                    if (listScores.get(j).actual == 1) {
                        if (newPreditions[j] == 1)
                            iTp++;
                        else
                            iFn++;
                    }// if actual == 1, true
                    else {
                        if (newPreditions[j] == 0)
                            iTn++;
                        else
                            iFp++;
                    } // if actual == 0, false
                }// for j, get i_tp, i_tn, i_fp, i_fn
                this.scoreThres = String.valueOf(thres);
                if ((iTp + iFp + iFn + iTn) != 0) {
                    this.accuracy = String.valueOf((iTp + iTn)/ (iTp + iFp + iFn + iTn));
                } else {
                    this.accuracy = "NA";
                }
                if ((iTp + iFp) != 0) {
                    this.precision = String.valueOf((iTp / (iTp + iFp)));
                } else {
                    this.precision = "NA";
                }
                if ((iTp + iFn) != 0) {
                    this.sensitivity = String.valueOf((iTp / (iTp + iFn)));
                } else {
                    this.sensitivity = "NA";
                }
                if ((iFp + iTn) != 0) {
                    this.specificity = String.valueOf((iTn / (iFp + iTn)));
                } else {
                    this.specificity = "NA";
                }
                if ((iTp + iFp) * (iTp + iFn) * (iTn + iFp)* (iTn + iFn) != 0) {
                    this.mcc = String.valueOf(((iTp * iTn - iFp * iFn) / Math
                            .sqrt((iTp + iFp) * (iTp + iFn) * (iTn + iFp)
                                    * (iTn + iFn))));
                } else {
                    this.mcc = "NA";
                }
                break;
            //endregion case score
            default:
                System.out.println("wrong parameter");
                break;
        }
    }
    
}
