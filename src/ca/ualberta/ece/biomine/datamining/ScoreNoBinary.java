package ca.ualberta.ece.biomine.datamining;

public class ScoreNoBinary implements Comparable<ScoreNoBinary>{
    public double actual, scoreForPos;
    public ScoreNoBinary(double actual, double scoreForPos){
        this.actual = actual;
        this.scoreForPos = scoreForPos;
    }//construction
    @Override
    public int compareTo(ScoreNoBinary c){ //should be sorted in descending order
        if(this.scoreForPos > c.scoreForPos)
            return -1;
        else if (this.scoreForPos == c.scoreForPos)
            return 0;
        else
            return 1;
    }
}
