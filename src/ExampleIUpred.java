import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import ca.ualberta.ece.biomine.datamining.*;

public class ExampleIUpred {
	/*
	 * this example reads the output score from IUPred on the "validateTest.csv" set, and evaluates the outputs
	 */
	public static void main(String[] args) throws Exception {
		// reads the native class label
		ArrayList<Double> listActual = new ArrayList<>();
		BufferedReader bfr = new BufferedReader(new FileReader("./data/validateTest.csv"));
		String line;
		int lineCount = 0;
		while((line = bfr.readLine()) != null){
			if(lineCount == 0){
				lineCount ++;
				continue; // skip the header line
			}
			String[] splited = line.split(",");
			double score = Double.valueOf(splited[splited.length - 1]); // last column is the class label
			listActual.add(score);
			lineCount ++;
		}
		bfr.close();
		
		// reads the predicted score from IUPred
		ArrayList<Double> listPredScore = new ArrayList<>();
		bfr = new BufferedReader(new FileReader("example_iupred.txt"));
		while((line = bfr.readLine()) != null){
			listPredScore.add(Double.valueOf(line));
		}
		bfr.close();
		
		// wrap actual label and predicted score
		if(listActual.size() != listPredScore.size()){
			System.out.println("error");
			return;
		}
		ArrayList<ScoreNoBinary> listScores = new ArrayList<>();
		for(int i = 0; i < listActual.size(); i ++){
			listScores.add(new ScoreNoBinary(listActual.get(i), listPredScore.get(i)));
		}
		listActual.clear();
		listPredScore.clear();
		
		// evaluate AUC, sensitivity, specificity and MCC. Sensitivity, specificity and MCC are evaluated by binary prediction where the false positive rate = 5%
		Evaluation eva1 = new Evaluation(listScores, "fpr", 0.05);
		// evaluate AUC, sensitivity, specificity and MCC. Sensitivity, specificity and MCC are evaluated by binary prediction where the predicted #positives = native #positives
		Evaluation eva2 = new Evaluation(listScores, "equal", Double.NaN);
		listScores.clear();
		System.out.println("AUC,sensitivity,specificity,mcc");
		System.out.println(eva1.auc + " " + eva1.sensitivity + " " + eva1.specificity + " " + eva1.mcc);
		System.out.println(eva2.auc + " " + eva2.sensitivity + " " + eva2.specificity + " " + eva2.mcc);
	}// main

}// class
